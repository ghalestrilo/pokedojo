from requests import get
import json
import pickle

with open('data.json', 'w') as storage:
    pokemons = []
    for i in range(100):
        r = get(f'https://pokeapi.co/api/v2/pokemon/{i+1}')
        pokemons.append(r.json())

    json.dump(pokemons, storage)
