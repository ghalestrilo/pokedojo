(defproject pokedojo "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://github.com/ghalestrilo/pokedojo"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [cheshire "5.8.0"]
                 [ring/ring-core "1.6.1"]
                 [ring/ring-devel "1.6.3"]
                 [ring/ring-jetty-adapter "1.6.3"]]
  :ring {:handler pokedojo.server/server}
  :plugins [[lein-ring "0.12.1"]
            [lein-ltest "0.4.0"]]
  :repl-options {:init-ns pokedojo.core}
  :main ^:skip-aot pokedojo.main)
