# pokedojo

An API designed for [Dia de Codar 2020](https://diadecodar.com.br)'s Clojure Pokemon API Dojo.

The language: (quite obviously) clojure

The setup:
- A `(pokedojo.core)` namespace with a few method signatures which we should implement
- A simple http server fully routed to run the methods within `(pokedojo.core)`
- A test suite for said methods (run by a self-reloading helper `test.sh`).

The challenge:

## Usage

FIXME

## License

Copyright © 2019 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
