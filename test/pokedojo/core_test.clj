(ns pokedojo.core-test
  (:require [clojure.test :refer :all]
            [clojure.set :refer [superset?]]
            [pokedojo.core :as core]))

(deftest test-ping
  (testing "ping"
    (let [{:keys [status body]} (core/ping "ping")]
      (->> status (= 200) is)
      (->> body (= "PONG") is))))

(deftest test-get-pokemon
  (let [pikachu {:name "pikachu" :id 1}
        something-else {:id 2}
        state [something-else pikachu]]
    (testing "when pokemon exists"
      (testing "gets pokemon by name"
        (let [req {:id "pikachu"}
              {:keys [status body]} (core/get-pokemon req state)]
          (testing "returns pokemon"
            (is (= pikachu body)))))
      (testing "gets pokemon by id"
        (let [req {:id 1}
              {:keys [status body]} (core/get-pokemon req state)]
          (is (= pikachu body)))))

    (testing "when pokemon does not exist"
      (let [req {:id "not-pikachu"}
            {:keys [status body]} (core/get-pokemon req state)]
        (is (= nil body))))))


(deftest test-pokemons-from-type
  (let [bulbasaur {:name "bulbasaur"
                   :types [{:type {:name "poison"}}
                           {:type {:name "grass"}}]}
        kakuna {:name "kakuna"
                :types [{:type {:name "poison"}}
                        {:type {:name "bug"}}]}
        state [bulbasaur kakuna]]

    (testing "when there are pokemons of given type"
      (testing "returns pokemons"
        (let [input {:type "grass"}
              {:keys [status body]} (core/pokemons-from-type input state)]
          (is (= [bulbasaur] body))))

      (testing "returns all pokemons from type"
        (let [input {:type "poison"}
              {:keys [status body]} (core/pokemons-from-type input state)]
          (is (= state body)))))

    (testing "when there are no pokemons of given type"
      (testing "returns no pokemons"
        (let [input {:type "fire"}
              {:keys [status body]} (core/pokemons-from-type input state)]
          (is (= [] body)))))))



(deftest test-pokemons-from-type
  (let [bulbasaur {:name "bulbasaur"
                   :types [{:type {:name "poison"}}
                           {:type {:name "grass"}}]}
        kakuna {:name "kakuna"
                :types [{:type {:name "poison"}}
                        {:type {:name "bug"}}]}
        state [bulbasaur kakuna]]

    (testing "when there are pokemons of given type"
      (testing "returns pokemons"
        (let [input {:type "grass"}
              {:keys [status body]} (core/pokemons-from-type input state)]
          (is (= [bulbasaur] body))))

      (testing "returns all pokemons from type"
        (let [input {:type "poison"}
              {:keys [status body]} (core/pokemons-from-type input state)]
          (is (= state body)))))

    (testing "when there are no pokemons of given type"
      (testing "returns no pokemons"
        (let [input {:type "fire"}
              {:keys [status body]} (core/pokemons-from-type input state)]
          (is (= [] body)))))))


(deftest test-evolutions
  (let [bulbasaur {:name "bulbasaur"
                   :species {:name "bulbasaur"}
                   :order 1}
        ivysaur {:name "bulbasaur"
                 :species {:name "bulbasaur"}
                 :order 2}
        venosaur {:name "bulbasaur"
                  :species {:name "bulbasaur"}
                  :order 3}
        state [venosaur bulbasaur ivysaur]]

    (testing "when species exist"
      (testing "returns evolution chain"
        (let [input {:name "bulbasaur"}
              {:keys [status body]} (core/pokemons-from-type input state)]
          (is (= [bulbasaur ivysaur venosaur] body)))))

    (testing "when species do not exist"
      (testing "returns empty vec"
        (let [input {:name "not-bulbasaur"}
              {:keys [status body]} (core/pokemons-from-type input state)]
          (is (= [] body)))))))



(deftest test-put-pokemon
  (let [bulbasaur {:name "bulbasaur"
                   :species {:name "bulbasaur"}
                   :order 1}
        ivysaur {:name "bulbasaur"
                 :species {:name "bulbasaur"}
                 :order 2}
        state [bulbasaur]]

    (testing "when input is nil - does not insert"
      (let [input {:name "bulbasaur"}
            [response newstate] (core/put-pokemon input state)]
        (is (= state newstate))))

    (testing "when input is ok - inserts new pokemon"
      (let [input {:name "bulbasaur"}
            [response newstate] (core/put-pokemon input state)]
        (is (= [bulbasaur ivysaur] newstate))))))