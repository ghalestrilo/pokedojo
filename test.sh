#!/bin/bash -eu
DIRECTORY_TO_OBSERVE="src test"      # might want to change this
function block_for_change {
  inotifywait --recursive \
    --event modify,move,create,delete \
    $DIRECTORY_TO_OBSERVE
}
function build {
  lein ltest
}
build
while block_for_change; do
  build
done