(ns pokedojo.main
  (:gen-class)
  (:require [pokedojo.server        :refer [server dev-server wrap-json]]
            [ring.adapter.jetty     :refer [run-jetty]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]))


(def run-server
  (-> server
      (wrap-keyword-params)
      (wrap-params)
      (wrap-json)
      (run-jetty {:port 3000})))

(def run-dev-server
  (-> dev-server
      (wrap-reload)
      (wrap-keyword-params)
      (wrap-params)
      (wrap-json)
      (run-jetty {:port 3000})))

; Main
(defn -main
  [& args]
  (run-server))
