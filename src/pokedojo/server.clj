(ns pokedojo.server
  (:require [pokedojo.core :as core]
            [cheshire.core :as cheshire]))

(defn readfile
  [filename & printdata]
  (let [data (cheshire/parse-string (slurp (str filename)) true)]
    (do (if printdata (println data))
        data)))


; (def initial-state {:hello "world"})

(def initial-state (readfile "data.json"))

(defn return [a]
  (fn [s] [a s]))

; Allow functions to optionally return new state
(defn respond [function a]
  (fn [s] (let [ret (function a s)]
            (if (seq? ret) ret [ret s]))))

(defn wrap-state
  ([handler] (wrap-state handler initial-state))
  ([handler initstate]
   (let [s (atom initstate)] ; State Atom
    ; New function, with state
     (fn [&request]
       (let [[response newstate] ((handler &request) @s)]
         (reset! s newstate) ; State is updated
         response)))))

; Server
(defn routes
  [req]
  (let [method (:request-method req)
        uri    (str (:uri req))]
    (case uri

      "/"
      (respond core/example req)

      "/ping"
      (respond core/ping req)

      "/pokemon"
      (respond core/get-pokemon req)

      "/evolutions"
      (respond core/evolutions req)

      "/pokemons-from-type"
      (respond core/pokemons-from-type req)

      "/put-pokemon"
      (respond core/put-pokemon req)


      ; Route not available
      :else
      (return core/not-found))))

; Our server is basically a state-aware version of routes
(def server (wrap-state routes initial-state))

(def dev-server (wrap-state routes initial-state))

; Exported to main, as outer-wrapper for our server's responses
(defn wrap-json
  [handler]
  (fn [request]
    (-> (handler request)
        (:body)
        (cheshire/generate-string)
        (ring.util.response/response)
        (assoc-in [:headers "Content-Type"] "application/json"))))
