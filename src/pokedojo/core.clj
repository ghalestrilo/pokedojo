(ns pokedojo.core
  (:require [pokedojo.core     :as core]
            [ring.util.response :refer [response]]))

(defn example
  [req state]
  (response (-> state first (dissoc :moves :game_indices))))

(defn ping
  [req & state]
  (response "PONG"))

(defn not-found
  [& whatever]
  (response {:status 404
             :body   "not found"}))



; TODO: Let's Code!

(defn get-pokemon [req state]
  (response "to be implemented!"))

(defn pokemons-from-type [req state]
  (response "to be implemented!"))

(defn evolutions [req state]
  (response "to be implemented!"))

(defn put-pokemon [req state]
  [(response "to be implemented!") state])