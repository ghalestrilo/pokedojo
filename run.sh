#!/bin/sh
DIRECTORY_TO_OBSERVE="src"      # might want to change this
while true; do
  $@ &
  PID=$!
  inotifywait --recursive \
    --event modify,move,create,delete \
    $DIRECTORY_TO_OBSERVE
  kill $PID
done